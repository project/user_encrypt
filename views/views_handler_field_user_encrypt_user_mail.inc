<?php
/**
 * @file
 * Field handler to provide access control for the encrypted email field.
 */

/**
 * A handler to display the uncrypted email version.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_user_encrypt_user_mail extends views_handler_field_user_mail {

  /**
   * Add data fields to query fields.
   */
  function query() {
    // Add this field to make it available in render.
    $table_alias = $this->getUserTableAlias($this->query->table_queue);
    $this->query->add_field($table_alias, 'data', 'users_data_encrypt');
    // In all cases, go through the parent default way.
    return parent::query();
  }

  /**
   * Decrypt data.
   */
  function render($values) {
    if ($values) {
      if (is_string($values->users_data_encrypt)) {
        $values->users_data_encrypt = unserialize($values->users_data_encrypt);
      }

      $field_name = $this->table . '_mail';
      $values->{$field_name} = user_encrypt_decrypt_data($values->users_data_encrypt['encrypt_mail']);
    }

    // In all cases, go through the parent default way.
    return parent::render($values);
  }

  /**
   * Get user table alias.
   */
  private function getUserTableAlias($table_queue) {
    foreach ($table_queue as $table_def) {
      if ($table_def['table'] == 'users') {
        return $table_def['alias'];
      }
    }
    // Still here? we return a reasonable default.
    return 'users';
  }

}
