user_encrypt
============

Drupal 7 module to encrypt the name, mail and init columns in core users table.

TODO Optimize: do not encrypt/decrypt a value twice.
TODO Implement views exposed filters (may be done via query alter).
TODO Anything else?
